# Dish Programing Language

### Q&A:
**What's the reason for naming it Dish?**
In short, it was supposed to be named Waffle, but I later found out that there�s already a language with the same name. So,
I named it Dish. Am I happy with it? Absolutely not!