﻿#include <cerrno>
#include <cstring>
#include <iostream>
#include <fstream>
#include <string>
#include "lexer/Scanner.h"

using std::cout;

int main() {
	std::string source_path = ("C:\\XboxGames\\dish-programing\\main.dish");
	Scanner scanner(source_path);
	TokenList tokens = scanner.scan_it();
	for (auto& token : tokens)
		cout << token.to_string() << "\n";
	return 0;
}