#pragma once

#include <sstream>
#include <string>
#include <fstream>
#include <iostream>
#include <optional>
#include <vector>
#include <map>
#include <format>
#include <cerrno>
#include <cstring>


enum TokenType {
	_EOF = -1,
	// Operators
	Minus, Plus, ForwardSlash, Star,
	// Leterals
	Natural_number, Real_number, String, Identifier,
	// Regesters
	Rg_main,
	// Keywords
	// Data Movement Instructions
	Incr, Decr, Set, Push, Pop, Move, Load_addr,
	// Arithmetic and Logic Instructions
	Add, Sub, Mul, Div, And, Or, Xor, Not,
	// Control Transfer Instructions
	Subrutine, Call, Return, Lable, Jump, Jump_equal, Jump_nequal,
	If,
	// Comparison Instructions
	Cmp, Test,
	// Conditional Instructions
	// String Instructions
	Moves, Loads, Stores, Scans,
	// Floating-Point Instructions
	Fadd, Fsub, Fmul, Fdiv
};

struct Token {
	TokenType type;
	std::string value;
	size_t line;
	size_t column;
	Token(TokenType type, std::string value, size_t line, size_t column): type(type), value(value), line(line), column(column)
	{}
	std::string to_string() const {
		std::ostringstream oss{};
		oss << "{ type: " << type << ", value: \"" << value << "\", line: " << line << ", column: " << column << " }";
		return oss.str();
	}
	bool operator==(Token const& other) const {
		return type == other.type;
	}
	bool operator==(TokenType const& other) const {
		return type == other;
	}
};

static void init_keywords(std::map<std::string, TokenType>& map) {
	// Data Movement Instructions
	map["incr"] = TokenType::Incr;
	map["decr"] = TokenType::Decr;
	map["set"] = TokenType::Set;
	map["move"] = TokenType::Move;
	map["push"] = TokenType::Push;
	map["pop"] = TokenType::Pop;
	map["load_addr"] = TokenType::Load_addr;

	// Arithmetic and Logic Instructions
	map["add"] = TokenType::Add;
	map["sub"] = TokenType::Sub;
	map["mul"] = TokenType::Mul;
	map["div"] = TokenType::Div;
	map["and"] = TokenType::And;
	map["or"] = TokenType::Or;
	map["Xor"] = TokenType::Xor;
	map["not"] = TokenType::Not;

	// Regesters
	map["rg_main"] = TokenType::Rg_main;

	// Control Transfer Instructions
	map["subrutine"] = TokenType::Subrutine;
	map["call"] = TokenType::Call;
	map["return"] = TokenType::Return;
	map["lable"] = TokenType::Lable;
	map["jump"] = TokenType::Jump;
	map["jump_equal"] = TokenType::Jump_equal;
	map["jump_nequal"] = TokenType::Jump_nequal;
	map["if"] = TokenType::If;

	// Comparison Instructions
	map["cmp"] = TokenType::Cmp;
	map["test"] = TokenType::Test;

	// Conditional Instructions
	// String Instructions
	map["moves"] = TokenType::Moves;
	map["loads"] = TokenType::Loads;
	map["stores"] = TokenType::Stores;
	map["scans"] = TokenType::Scans;

	// Floating-Point Instructions
	map["fadd"] = TokenType::Fadd;
	map["fsub"] = TokenType::Fsub;
	map["fmul"] = TokenType::Fmul;
	map["fdiv"] = TokenType::Fdiv;
}

typedef std::vector<Token> TokenList;

class Scanner {
private:
	size_t column;
	size_t current_line_number;
	size_t start;
	std::ifstream source_code;
	std::string current_line;
	TokenList tokens;
	
	char advance() {
		return current_line[column++];
	}
	char peek() {
		if (at_end_line()) return '\0';
		return current_line[column];
	}
	char peekNext() {
		if (current_line.length() > column+1)
			return current_line[column + 1];
		return '\0';
	}
	bool at_end_line() {
		return column >= current_line.length();
	}
	bool match(char expected) {
		if (at_end_line() || current_line[column] != expected) return false;

		column++;
		return true;
	}
	void add_token(TokenType type) {
		std::string text = type == TokenType::_EOF ? "" : current_line.substr(start, column - start);
		tokens.push_back(Token(type, text, current_line_number, start+1));
	}
	bool is_alpha(char c) {
		return (c >= 'a' && c <= 'z') ||
			(c >= 'A' && c <= 'Z') ||
			c == '_';
	}
	bool is_alpha_numeric(char c) {
		return is_alpha(c) || is_digit(c);
	}
	bool is_digit(char c) {
		return c >= '0' && c <= '9';
	}

	void scan_line() {
		while (!at_end_line()) {
			start = column;
			char c = advance();
			switch (c) {
				case ' ':
				case '\r':
				case '\t':
				case '\n':
					// Ignoring white spaces
					break;
				case '*': add_token(TokenType::Star); break;
				case '+': add_token(TokenType::Plus); break;
				case '-': add_token(TokenType::Minus); break;
				case '/':
					if (match('/'))
						while (!at_end_line()) advance();
					else
						add_token(TokenType::ForwardSlash);
					break;
				default:
					if (is_digit(c))
						number();
					else if (is_alpha(c))
						identifier();
					else {
						std::cerr << "Unexpected character";
						exit(1);
					}
					break;
			}
		}
	}

	void number() {
		while (is_digit(peek()) || peek() == '.') advance();
		add_token(TokenType::Natural_number);
	}

	void identifier() {
		char c;
		while (is_alpha_numeric((c = peek()))) advance();
		std::string text = current_line.substr(start, column - start);
		if (keywords.find(text) != keywords.end())
			add_token(keywords[text]);
		else
			add_token(TokenType::Identifier);
	}
public:
	std::map<std::string, TokenType> keywords;
	Scanner(std::string path) {
		init_keywords(keywords);
		source_code.open(path);
		if (!source_code.is_open()) {
			std::cerr << "Error opening file: " << strerror(errno) << std::endl;
			std::exit(1);
		}
		current_line_number = 0;
		column = 0;
	}
	TokenList scan_it() {
		while (std::getline(source_code, current_line)) {
			current_line_number++;
			column = 0;
			scan_line();
		}
		add_token(TokenType::_EOF);
		return tokens;
	}
};